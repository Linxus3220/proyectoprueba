package co.com.choucair.certification.proyectoPrueba.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchCoursePage {

    public static final Target BUTTON_UNI = Target.the("Seleccionar Choucair").located(By.xpath("//div[@id='certificaciones']"));
    public static final Target INPUT_COURSE = Target.the("buscar curso").located(By.id("coursesearchbox"));
    public static final Target BUTTON_GO = Target.the("da click para buscar el curso").located(By.xpath("//button[@class='btn btn-secondary']"));
    public static final Target SELECT_COURSE = Target.the("da click en el curso").located(By.xpath("/html/body/div[2]/div[2]/div/div/div/section/div/div/div[1]/h4/a"));
    public static final Target NAME_COURSE = Target.the("extrae el nombre del curso").located(By.xpath("/html/body/div[1]/div[2]/div/header/div/div/div[2]/div[1]/div[1]/a/div/div/h1"));
}
